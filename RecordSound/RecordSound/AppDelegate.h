//
//  AppDelegate.h
//  RecordSound
//
//  Created by ddominguezh on 28/11/13.
//  Copyright (c) 2013 quaternary. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
