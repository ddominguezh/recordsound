//
//  ViewController.h
//  RecordSound
//
//  Created by ddominguezh on 28/11/13.
//  Copyright (c) 2013 quaternary. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>

@interface ViewController : UIViewController<AVAudioRecorderDelegate, AVAudioPlayerDelegate, NSURLSessionDelegate, NSURLSessionDataDelegate>

@property (strong, nonatomic) NSURL *soundFileURL;
@property (strong, nonatomic) AVAudioRecorder *recorder;
@property (strong, nonatomic) AVAudioPlayer *player;

- (IBAction)grabar:(id)sender;
- (IBAction)reproducir:(id)sender;
- (IBAction)parar:(id)sender;
- (IBAction)subir:(id)sender;

@end
