//
//  ViewController.m
//  RecordSound
//
//  Created by ddominguezh on 28/11/13.
//  Copyright (c) 2013 quaternary. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    NSArray *dirPaths;
    NSString *docsDir;
    
    dirPaths = NSSearchPathForDirectoriesInDomains(    NSDocumentDirectory, NSUserDomainMask, YES);
    docsDir = dirPaths[0];
    
    NSString *soundFilePath = [docsDir stringByAppendingPathComponent:@"sound.acc"];
    
    _soundFileURL = [NSURL fileURLWithPath:soundFilePath];
    
    NSDictionary *recordSettings = [NSDictionary dictionaryWithObjectsAndKeys:
                                    [NSNumber numberWithInt:AVAudioQualityMin], AVEncoderAudioQualityKey,
                                    [NSNumber numberWithInt: 1], AVNumberOfChannelsKey,
                                    [NSNumber numberWithFloat:44100.0], AVSampleRateKey,
                                    [NSNumber numberWithInt:kAudioFormatMPEG4AAC], AVFormatIDKey,
                                    nil
                                    ];
    
    NSError *error = nil;
    
    AVAudioSession *audioSession = [AVAudioSession sharedInstance];
    [audioSession setCategory:AVAudioSessionCategoryPlayAndRecord
                        error:nil];
    
    _recorder = [[AVAudioRecorder alloc]
                      initWithURL:_soundFileURL
                      settings:recordSettings
                      error:&error];
    
    if (error)
    {
        NSLog(@"error: %@", [error localizedDescription]);
    } else {
        [_recorder prepareToRecord];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)grabar:(id)sender {
    if (!_recorder.recording)
    {
        [_recorder record];
    }
}

- (IBAction)reproducir:(id)sender {
    if(!_recorder.recording){

        NSError *error;
        
        _player = [[AVAudioPlayer alloc]
                        initWithContentsOfURL:_soundFileURL
                        error:&error];
        
        _player.delegate = self;
        
        if (error)
            NSLog(@"Error: %@",
                  [error localizedDescription]);
        else
            [_player play];
    }
}

- (IBAction)parar:(id)sender {
    if(_recorder.recording){
        [_recorder stop];
    }else if(_player.playing){
        [_player stop];
    }
}

- (IBAction)subir:(id)sender {
    NSURL *url = [NSURL URLWithString:@"http://quaternary.es/uploadTest/upload.php"];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request setHTTPMethod:@"POST"];

    NSURLSession *urlSeesion = [NSURLSession sessionWithConfiguration:[ViewController createNSURLSessionConfiguration:@"application/json"] delegate:self delegateQueue:[NSOperationQueue mainQueue]]
    ;
    
    NSData *data = [NSData dataWithContentsOfURL:_soundFileURL];
    NSString * params = [NSString stringWithFormat:@"file=%@&", @"musica.mp3"];
    
    [request setHTTPBody:[params dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSURLSessionUploadTask *uploadTask = [urlSeesion uploadTaskWithRequest:request fromData:data completionHandler: ^(NSData *data, NSURLResponse *response, NSError *error){
        if (error == nil) {
            
        }else{
            NSLog(@"Error en la llamada a subir el sonido: ERRROR----> %@", error.description);
        }
    }];
    [uploadTask resume];
    
    /*NSString *deviceID = [[NSUserDefaults standardUserDefaults] objectForKey:@"deviceID"];
    NSString *textContent = @"Esto es una nueva nota";
    
    // Preparamos el cuerpo (body) de la solicitud
    NSString *boundary = @"SendoaPortuondoFormBoundary";
    NSMutableData *body = [NSMutableData data];
    // Parte del body para Device ID
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", @"deviceId"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"%@\r\n", deviceID] dataUsingEncoding:NSUTF8StringEncoding]];
    // Parte del body para Text content
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", @"textContent"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"%@\r\n", textContent] dataUsingEncoding:NSUTF8StringEncoding]];
    // Parte del body para la imagen
    NSData *data = [NSData dataWithContentsOfURL:_soundFileURL];
    if (data) {
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"; filename=\"musica.mp3\"\r\n", @"mp3"] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[@"Content-Type: audio/mpeg\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:data];
        [body appendData:[[NSString stringWithFormat:@"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    }
    [body appendData:[[NSString stringWithFormat:@"--%@--\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    
    // Configuración de la sesión
    NSURLSessionConfiguration *sessionConfiguration = [NSURLSessionConfiguration defaultSessionConfiguration];
    sessionConfiguration.HTTPAdditionalHeaders = @{
                                                   @"api-key"       : @"55e76dc4bbae25b066cb",
                                                   @"Accept"        : @"application/json",
                                                   @"Content-Type"  : [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary]
                                                   };
    
    // Creación de la sesión
    // Asignando un delegado (NSURLSessionTaskDelegate) podremos seguir el progreso de la subida de archivo
    NSURLSession *session = [NSURLSession sessionWithConfiguration:sessionConfiguration delegate:self delegateQueue:nil];
    
    // Tarea de subida de datos
    NSURL *url = [NSURL URLWithString:@"http://quaternary.es/uploadTest/upload.php"];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    request.HTTPMethod = @"POST";
    request.HTTPBody = body;
    NSURLSessionDataTask *uploadTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        // Sondeo de la respuesta HTTP del servidor
        NSHTTPURLResponse *HTTPResponse = (NSHTTPURLResponse *)response;
        if (HTTPResponse.statusCode == 200) {
            // Conversión de JSON a objeto Foundation (NSDictionary)
            NSError *JSONError;
            NSDictionary *responseBody = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&JSONError];
            if (!JSONError) {
                NSLog(@"Response body: %@", responseBody);
                dispatch_async(dispatch_get_main_queue(), ^{
                    // Actualizar UI según como consideremos oportuno
                });
            } else {
                NSAssert(NO, @"Error en la conversión de JSON a Foundation");
            }
        } else {
            NSAssert(NO, @"Error a la hora de obtener las notas del servidor");
        }
    }];
    [uploadTask resume];*/
    
}

-(void) httpPostWithCustomDelegate
{
    NSURLSessionConfiguration *defaultConfigObject = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *defaultSession = [NSURLSession sessionWithConfiguration: defaultConfigObject delegate: nil delegateQueue: [NSOperationQueue mainQueue]];
    
    NSURL * url = [NSURL URLWithString:@"http://hayageek.com/examples/jquery/ajax-post/ajax-post.php"];
    NSMutableURLRequest * urlRequest = [NSMutableURLRequest requestWithURL:url];
    NSString * params =@"name=Ravi&loc=India&age=31&submit=true";
    [urlRequest setHTTPMethod:@"POST"];
    [urlRequest setHTTPBody:[params dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSURLSessionDataTask * dataTask =[defaultSession dataTaskWithRequest:urlRequest
                                                       completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                           NSLog(@"Response:%@ %@\n", response, error);
                                                           if(error == nil)
                                                           {
                                                               NSString * text = [[NSString alloc] initWithData: data encoding: NSUTF8StringEncoding];
                                                               NSLog(@"Data = %@",text);
                                                           }
                                                           
                                                       }];
    [dataTask resume];
    
}

+ (NSURLSessionConfiguration *) createNSURLSessionConfiguration:(NSString *) accept{
    NSString *boundary = @"SendoaPortuondoFormBoundary";
    NSURLSessionConfiguration *nsURLSessionConfig = [NSURLSessionConfiguration defaultSessionConfiguration];
    nsURLSessionConfig.allowsCellularAccess = NO;
    nsURLSessionConfig.timeoutIntervalForRequest = 90.0;
    nsURLSessionConfig.timeoutIntervalForResource = 120.0;
    nsURLSessionConfig.HTTPMaximumConnectionsPerHost = 1;
    [nsURLSessionConfig setHTTPAdditionalHeaders:@{
                                                   @"Accept": accept,
                                                   @"Content-Type"  : [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary]
                                                }];
    
    return nsURLSessionConfig;
}

/*
 
 
 */
@end
